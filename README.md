# CNC Parallel Interface

Parallel port interface for [type] CNC Lathe and Mill

## CNC Lathe I/O Mapping

| Pport Pin | OUT XY En   | OUT  Act En | IN(p16 Low)          | IN(p16 High) | BP LEDs |
|-----------|-------------|-------------|----------------------|--------------|---------|
| 1         | XY Enable   | XY Enable   |                      |              |         |
| 2         | M1 X+       |             |                      |              | OUT 1   |
| 3         | M1 X-       | Door        |                      |              | OUT 2   |
| 4         |             | Tailstock   |                      |              | OUT 3   |
| 5         |             |             |                      |              | OUT 4   |
| 6         | M3 Z+       | Air blast   |                      |              | OUT 5   |
| 7         | M3 Z-       |             |                      |              |         |
| 8         |             |             |                      |              |         |
| 9         |             | Spindle     |                      |              |         |
| 10        |             |             | Door (0 Open)        |              | IN2     |
| 11        |             |             |                      | Z encoder    |         |
| 12        |             |             |                      |              |         |
| 13        |             |             | Air Pressure (1 Low) | X encoder    | IN3     |
| 14        | Actuator En | Actuator En |                      |              |         |
| 15        |             |             |                      |              |         |
| 16        | Select In   | Select In   |                      |              |         |
| 17        | Motor Relay | Motor Relay |                      |              |         |

Motor relay needs to be activated (1) for any motor operation  
Set P1-9, 14 are active low, set to high before any operation

## PCB
PCB is designed as a single layer with jumper wires for easy DIY manufacturing but is also suitable for 2 layer fabrication.

![CNC Board](CNC_Interface.png)
![CNC Board](CNC_Interface_1.png)
